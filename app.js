var   express = require('express'),
          app = express(),
         http = require('http'),
       server = http.createServer(app),
           io = require('socket.io').listen(server),
   expressHbs = require('express3-handlebars'),
           fs = require('fs'),
       config = require(__dirname + '/config'),
  TwStreamApp = require(__dirname + '/app/tw_stream_app');
        dbUrl = "art_fest", // "username:password@example.com/art_fest"
  collections = ["users", "tweets"],
           db = require("mongojs").connect(dbUrl, collections)


function setup_handlebars() {
  app.engine('hbs', expressHbs({
    layoutsDir: __dirname + '/views/layouts',
    extname:'hbs',
    defaultLayout:'main.hbs'
  }));

  app.get('/', function (req, res) {
    res.render('node-twitter-stream.hbs', {
      page_title: Math.random(),
      env: (process.env.NODE_ENV || 'development'),
      config: config
    });
  });
}

setup_handlebars();

// app.get('/node-twitter-stream.css', function (req, res) {
//   res.sendfile(__dirname + '/app/assets/style.css');
// });

app.use(express.static(__dirname + '/public'));

TwStreamApp(config, io);

// io.sockets.on('connection', function (socket) {
//   // body...
// });

console.log("Listening at port: " + config.port + " for domain: " + config.domain);
server.listen(config.port);



