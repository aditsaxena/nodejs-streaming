// var databaseURI = "localhost:27017/somedb";
// var collections = ["users", "blogs"];
// var db = require("mongojs").connect(databaseURI, collections);
// module.exports = db;
// // --------
// var db = require("./db");
// // --------


var     dbUrl = "art_fest", // "username:password@example.com/art_fest"
  collections = ["users", "tweets"],
           db = require("mongojs").connect(dbUrl, collections)

db.users.save({email: "user@example.com", password: "the_pass"}, function(err, saved) {
  if( err || !saved ) console.log("User not saved");
  else console.log("User saved");
});

db.users.find({sex: "female"}, function(err, users) {
  if( err || !users) console.log("No female users found");
  else users.forEach( function(femaleUser) {
    console.log(femaleUser);
  } );
});

db.users.update({email: "srirangan@gmail.com"}, {$set: {password: "iReallyLoveMongo"}}, function(err, updated) {
  if( err || !updated ) console.log("User not updated");
  else console.log("User updated");
});
