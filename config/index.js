var _ = require('underscore');

module.exports = _.extend(
  require('./' + (process.env.NODE_ENV || 'development') + '.json'),
  require('./config.json')
);

