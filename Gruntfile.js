'use strict';

var config = require('./config');

module.exports = function(grunt) {

  grunt.initConfig({
      pkg: grunt.file.readJSON('package.json'),

      sass: {
        dist: {
          options: {
            includePaths: ['bower_components'], //, 'app/assets/style'],
            outputStyle: 'compressed'
          },
          files: {
            'public/css/app.css': [
              // 'bower_components/foundation/css/foundation.css',
              'app/assets/style/**/*.scss'
            ]
          }
        }
      },

      uglify: {
        build: {
          options: {
            // banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
            mangle: false,
            compress: false
          },
          files: {
            'public/js/app.js': [
              'bower_components/raphael/raphael-min.js',
              'app/assets/js/**/*.js'
            ]
          }
        }
      },

      watch: {
        styles: {
          files: 'app/assets/style/**/*.scss',
          tasks: ['sass']
        },
        scripts: {
          files: 'app/assets/js/**/*.js',
          tasks: ['uglify']
        }
      },

      ec2: {
        "AWS_ACCESS_KEY_ID": config.aws.access_key_id,
        "AWS_SECRET_ACCESS_KEY": config.aws.secret_access_key,
        "AWS_SECURITY_GROUP": config.aws.security_group,
        "AWS_INSTANCE_TYPE": config.aws.instance_type,
        "AWS_IMAGE_ID": config.aws.image_id,
        "AWS_DEFAULT_REGION": config.aws.default_region,
        // "AWS_ACCESS_KEY_ID": "AKIAIH5VYGHSPXGNUAVQ",
        // "AWS_SECRET_ACCESS_KEY": "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",
        // "AWS_SECURITY_GROUP": "basic-web", // "sg-32b57857",
        // "AWS_INSTANCE_TYPE": "t1.micro",
        // "AWS_IMAGE_ID": "ami-896c96fe", // Ubuntu Server 14.04 LTS (PV)
        // "AWS_DEFAULT_REGION": "eu-west-1",
        "VERBOSITY_NPM": 'debug',
        "VERBOSITY_RSYNC": 'vv'
      }
  });

  // grunt.loadNpmTasks('grunt-contrib-coffee');
  grunt.loadNpmTasks('grunt-sass');

  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.loadNpmTasks('grunt-ec2');

  grunt.registerTask('default', ['watch']);
};

