var Twit = require('twit');

// exports = module.exports; exports.application = function () { }

module.exports = function (config, io) {

  var T = new Twit({
           consumer_key: config.consumer_key,
        consumer_secret: config.consumer_secret,
           access_token: config.access_token,
    access_token_secret: config.access_token_secret
  });


  function stream_test() {
    setTimeout(function() {

        io.sockets.emit('stream', "turl " + Math.random(), "tweet.user.screen_name", "tweet.user.profile_image_url", "mediaUrl");

        stream_test();
    }, 5000);
  }

  io.sockets.on('connection', function (socket) {

    // var stream = T.stream('statuses/filter', { track: ['life'] })
    // //var stream = T.stream('statuses/sample') // Firehose (sampling of all Tweets)
    // //var stream = T.stream('user') // Your user stream

    // stream.on('tweet', function (tweet) {
    //   // Makes a link the Tweet clickable
    //   var turl = tweet.text.match( /(http|https|ftp):\/\/[^\s]*/i )
    //   if ( turl != null ) {
    //     turl = tweet.text.replace( turl[0], '<a href="'+turl[0]+'" target="new">'+turl[0]+'</a>' );
    //   } else {
    //     turl = tweet.text;
    //   }
    //   var mediaUrl;
    //   // Does the Tweet have an image attached?
    //   if ( tweet.entities['media'] ) {
    //     if ( tweet.entities['media'][0].type == "photo" ) {
    //       mediaUrl = tweet.entities['media'][0].media_url;
    //     } else {
    //       mediaUrl = null;
    //     }
    //   }

    //   io.sockets.emit('stream', turl, tweet.user.screen_name, tweet.user.profile_image_url, mediaUrl);

    // });
    
    stream_test();

  });

};

