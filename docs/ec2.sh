# ---:: INSTALL NGINX :: ---

sudo apt-get -y update && sudo apt-get -y upgrade
sudo apt-get -y install make nginx git

echo 'server {
    listen       80;
    server_name  ec2-54-76-101-186.eu-west-1.compute.amazonaws.com;

    location / {
        proxy_pass  http://127.0.0.1:4040/;
    }
}' > ~/tmp2/nginx.conf

sudo rm /etc/nginx/sites-enabled/default
sudo mv ~/tmp2/nginx.conf /etc/nginx/sites-enabled/nginx_conf.web
# sudo ln -s #{current_release}/config/nginx_conf.web /etc/nginx/sites-enabled/nginx_conf.web

sudo service nginx restart
